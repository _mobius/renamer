#!/usr/bin/python3

# 
# Copyright 2020-2022 Z. Brooks [gitlab.com/_mobius] 
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as 
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
#

"""renamer

Usage: renamer [-a | -n | -c] [-vpyfdstix] [--dir=<dir>] [--glob=<glob>] <match> [<replace> ...]
       renamer [-a | -n | -c] [-vpyfdstior] [--dir=<dir>] [--glob=<glob>] [--date=<date>] [--zpi=<size>] <match> <format>
       renamer [-a | -n | -c] [-vpyfdstix] [-l | -u] [--dir=<dir>] [--glob=<glob>] [<match>]

Options:
    -f --ignore-files     Ignore files matched
    -d --ignore-dirs      Ignore directories matched
    -s --ignore-links     Ignore symbolic links matched
    -i --ignore-case      Ignore case. Will force regex matching
    -t --dry-run          Print expected result but don't change names

    -x --regex            Use regex matching

    -u --upper            Change name to all uppercase
    -l --lower            Change name to all lowercase
 
    -a --print-unchanged  Print a list of all files processed
    -n --print-none       Don't print any file names, including changes
    -p --print-path       Print full path for each file
    -y --no-tree          Print file on each line without indenting or decoration
    -c --print-changes    Print only the changed name

    -v --verbose

    -g --glob=<glob>      Glob used to grab files [default: *]
    -w --dir=<dir>        Working directory [default: .]

    -o --format           Format in python format style. Uses regex matching
                          Available inserts for format:
                            {name} original name minus the suffixes
                            {stem} original name up to the last suffix
                            {ext} / {suffix} the last .xyz extension
                            {exts} / {suffixes} all.extensions
                            {index} index in file list 
                            {zpi} zero padded index
                            {date} modified date
                            {time} modified time
                            {ext#} where # is extension from the end.
                                example: abc.1.2.3 -> {ext0}=.3 {ext-1}=.2.3 {ext-2}=.1.2.3

    --date=<date_format>  Date format string for use with format [default: %Y-%m-%d]
                          using python time formating
    --time=<time_format>  Time format string [default: %H%M%S]

    -z --zpi=<size>       Minimum digits when padding index [default: 1]
                          Uses the number of files or the above padding, whichever is larger

    -r --group            Index is counted for files with similar final names and reset for each group

    -h --help             Print usage information
    --version             Print version

    <replace> is treated as regex if --re is specified instead of <search>
    Prints the matched files if <replace> is left out

Examples:
    Using: 
        file_abc_01_01.txt
        file_abc_01_02.txt

    Replace `abc` with xyz
        renamer abc xyz
            file_xyz_01_01.txt
            file_xyz_01_02.txt
    
    Replace 01 with [x,y]
        renamer 01 x y 
            file_abc_x_y.txt
            file_abc_x_02.txt

    Regex
        renamer -x 'file|abc' 'xyz'
            xyz_xyz_01_01.txt
            xyz_xyz_01_02.txt

    Format:
        renamer -o '(file).*(?P<num>0[0-2]_0[0-2])' '{0}_{num}{suffix}'
        renamer -o '(file).*(0[0-2]_0[0-2])' '{0}_{1}{suffix}'
        renamer -o '(file).*((0\d_?){2})' '{0}_{1}{suffix}'
            file_01_01.txt
            file_01_02.txt
    
"""

import os
import sys
import regex

from timeit import default_timer
from datetime import datetime, timezone
from pathlib import Path

import docopt

def pad_name( path, file, name ):
    """
        Pad filename/path so that it lines up nicely when printed
    """
    rel = file.relative_to( path )
    depth = (str( rel ).count( '/' ) + 2) * 2
    marker = '|+ ' if file.is_dir() else '|- '

    return f'{marker:>{depth}}{name}'

def print_files( args, path, files, changes = {} ):
    if args['--print-none']:
        return 0

    if not args['--print-changes']:
        print( path )
            
    changed = 0
    for f in files:
        rel_f = f if args['--print-path'] else f.relative_to( path )

        if f in changes and changes[f]:
            if args['--print-changes']:
                chp = path / changes[f]
                chp = chp if args['--print-path'] else chp.relative_to( path )

                if args['--no-tree']:
                    print( chp )
                else:
                    print( pad_name( path, f, chp ) )
            elif args['--no-tree']:
                print( f'{rel_f} -> {changes[f]}' )
            else:
                padded = pad_name( path, f, rel_f )
                print( f'{padded} -> {changes[f]}' )
            changed += 1
        elif not args['--print-changes'] and args['--print-unchanged']:
            if args['--no-tree']:
                print( str( rel_f ) )
            else:
                print( pad_name( path, f, rel_f ) )

    return changed

#
#   RegEx
#
def regex_single( file, rx, replace, replen ):
    name = file.name

    if replen > 1:
        for index in range( 0, replen - 1 ):
            name = rx.sub( replace[index], name, 1 )
        
    name = rx.sub( replace[-1], name )

    return name if name != file.name else None

def regex_mode( args, files ):
    """
        Functions like normal match/replace mode but using regex for pattern matching.

        Find replacement string in name with regex. 
        Modes:
        1. Swap [<replace>...] with capture groups
        2. Use --format if provided and pass in capture groups as arguments
    """
 
    rx = regex.compile( args['<match>'] ) if not args['--ignore-case'] else regex.compile( args['<match>'], regex.IGNORECASE )

    if args['--lower']:
        return dict( zip( files, [ f.name.lower() for f in files if rx.search( f.name ) ] ))
    elif args['--upper']:
        return dict( zip( files, [ f.name.upper() for f in files if rx.search( f.name ) ] ))

    replen = len( args['<replace>'] )
    if not replen:
        return {}

    return dict( zip( files, [ regex_single( f, rx, args['<replace>'], replen ) for f in files ] ) )


#
#   Normal REPLACE
#
def repn_single( file, match, replace, replen ):
    name = file.name

    if replen > 1:
        for index in range( 0, replen - 1 ):
            name = name.replace( match, replace[index], 1 )
        
    name = name.replace( match, replace[-1] )

    return name if name != file.name else None

def replace_normal( args, files ):
    """
        Normal replace mode where <match> is replaced with <replace>...
        * If there are more matches then <replace> arguments the last one is 
          used for the remainder
    """
    if args['--lower']:
        return dict( zip( files, [ f.name.lower() for f in files if args['<match>'] in f.name ] ) )
    elif args['--upper']:
        return dict( zip( files, [ f.name.upper() for f in files if args['<match>'] in f.name ] ) )

    replen = len( args['<replace>'] )
    if not replen:
        return {}

    return dict( zip( files, [ repn_single( f, args['<match>'], args['<replace>'], replen ) for f in files ] ) )

#
#   Format rename
#
def regex_format( args, files ):
    """
        Replace the string using .format
        * Non-matched parts of the name are saved in order with matched parts
    """
    try:
        rx = regex.compile( args['<match>'] ) if not args['--ignore-case'] else regex.compile( args['<match>'], regex.IGNORECASE )
    except Exception as e:
        print( e )
        sys.exit( 'Regex failed to compile.' )

    fmt_str = args['<format>']

    index = 0
    index_pad = max( int(args['--zpi']), len( str( len( files ) ) ) )

    groups = {} # track filename collisions & grouping

    changes = {}
    for file in files:
        name = file.stem + file.suffix

        suffixes = ''.join( file.suffixes )

        no_suffix = name[:-len(suffixes)]

        pre = {
            'name': no_suffix,       # name without any suffixes
            'stem': file.stem,  # name up to the last suffix
            'ext': file.suffix,
            'suffix': file.suffix, # last.suffix
            'exts': suffixes,
            'suffixes': suffixes, # file.all.the.suffixes
            'mdt': file.stat().st_mtime,
            'date': datetime.fromtimestamp( file.stat().st_mtime ).strftime( args['--date'] ), # st_mtime is a timestamp. convert to datetime object
            'index': index,
            'zpi': f'{index:0{index_pad}}',
        }

        
        suf_str = ''
        for i, suf in reversed( list( enumerate( file.suffixes ) ) ):
            suf_str = suf + suf_str
            pre[f'ext{i-len(file.suffixes) + 1}'] = suf_str


        # group indexing requires 2 step replacement
        if args['--group']:
            pre['index'] = '{gindex}'
            pre['zpi'] = '{gpi}'

        rslt = rx.search( name )

        if rslt:
            name = fmt_str.format( *rslt.groups(), **rslt.groupdict(), **pre )

            if args['--group']: # grouped by name, indexing
                if name in groups:
                    groups[name] += 1
                else:
                    groups[name] = 0

                pre['gindex'] = groups[name]
                pre['gpi'] = f'{groups[name]:0{index_pad}}'

                name = name.format( **pre ) 

                changes[file] = name

            else: # normal indexing behavior
                if name != file.name:
                    changes[file] = name

                index += 1

    return changes



def apply_changes( changes ):
    """
        Apply filename changes
            * Abort on name collisions
    """
    name_check = {}
    collisions = 0
    for file, name in changes.items():
        if name in name_check and name != None:
            print( f'Error: file name collision: {name}\n\t{name_check[name]}\n\t{file}' )
            collisions += 1
        else:
            name_check[name] = file

    if collisions > 0:
        print( f'Total Collisions: {collisions}' )
        return False

    for file,name in changes.items():
        if name:
            np = Path( file.parents[0] ).joinpath( name )
            file.rename( np )

    return True

def main( args ):

    start = default_timer()

    # change ~ to /home/??, change . or .. to absolute path
    path = Path( args['--dir'] ).expanduser().resolve()
    all_files = sorted( path.glob( args['--glob'] ) )

    files = all_files
    file_count = len( files )

    if args['--verbose']:
        print( f'Processing {file_count} files in {path}' )

    if args['--ignore-files']:
        files = [ f for f in files if not f.is_file() ]
        if args['--verbose']:
            print( f'Ignoring {file_count - len( files )} files.' )
        file_count = len( files )

    if args['--ignore-dirs']:
        files = [ d for d in files if not d.is_dir() ]
        if args['--verbose']:
            print( f'Ignoring {file_count - len( files )} directories.' )
        file_count = len( files )

    if args['--ignore-links']:
        files = [ l for l in files if not l.is_symlink() ]
        if args['--verbose']:
            print( f'Ignoring {file_count - len( files )} links.' )
        
    # create dictionary of potential changes
    changes = {}

    if args['--format']:
        changes = regex_format( args, files )
    elif args['--regex']:
        changes = regex_mode( args, files )
    else:
        if args['--ignore-case']:
            changes = regex_mode( args, files )
        else:
            changes = replace_normal( args, files )

    success = True
    if not args['--dry-run']:
        success = apply_changes( changes )
    else:
        print( 'Dry Run: No changes will occur.' )

    end = default_timer()

    file_count = len( changes )
    if success:
        change_count = print_files( args, path, files, changes )

    duration = end - start
    if args['--verbose']:
        print( f'Processed {file_count} and changed {change_count} files in {duration:.2g} seconds' )

    return 0
    

if __name__ == '__main__':
    main( docopt.docopt( __doc__ , argv=sys.argv[1:], version = 'renamer 1.2.2' ) )
