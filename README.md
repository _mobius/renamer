File renaming utility named **renamer** because I'm terrible at naming.

## Using

#### Replace
This is a simple match/replace mode. It will search for files with the given substring and then replace any instances with the given replacement values.

Replace spaces with underscore:
* files: `file 1.txt`
```
$ rnm ' ' '_'
file 1.txt -> file_1.txt
```

Replace multiple instances of the match string with different strings:
* file: `a_a_a`
```
$ rnm 'a' a b
a_a_a -> a_b_b
```

#### Regex
This mode functions like the **replace** mode above but uses *regex* to find matches.
**File:** `abc_01.txt` `xyz_02.txt`
```
$ rnm -x 'abc|xyz' 'file'
abc_01.txt -> file_01.txt
xyz_02.txt -> file_02.txt
```

#### Format
Use regex for pattern matching and string formatting to generate the final result.
**File:** `abc_1.txt` `xyz_2.txt`
```
$ rnm -o '.*(\d)' 'file_{0}{suffix}'
abc_01.txt -> file_01.txt
xyz_02.txt -> file_02.txt
```

Add a number to the file name using `{index}` or `{zpi}` (zero padded index).
**Files:** 'abc' 'xyz'
```
$ rnm -o '.*' 'file_{zpi}'
abc -> file_1
xyz -> file_2
```

The length of the zero padded index is determined by the number of file names changed. This can be changed with `--zpi`/`-z`:
```
$ rnm -o -z3 '.*' 'file_{zpi}'
abc -> file_001
xyz -> file_002
```

The modified *datetime* can be inserted into the name change with `{date}` and customized through `--date`:
**Files:** `f_1` `f_2` `f_3` `f_4`
```
$ rnm -o --date='%Y%m%d' '.*' '{date}'
f_1 -> 20220101
f_2 -> 20220101
f_3 -> 20220102
f_4 -> 20220102
```
The above example results in name collisions. File names that collide can "grouped" using the `{index}` or `{zpi}` options in the format string and the `-r` flag:
```
$ rnm -or --date='%Y%m%d' '.*' '{date}_{zpi}'
f_1 -> 20220101_0
f_2 -> 20220101_1
f_3 -> 20220102_0
f_4 -> 20220102_1
```